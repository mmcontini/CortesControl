﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CortesControl
{
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Provides an extension method that gets an iterator for
        /// IEnumerables. This makes it as easy to get an iterator as it 
        /// is to get an enumerator.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static IIterator<T> GetIterator<T>(this IEnumerable<T> enumerable)
        {
            return EnumeratorIterator<T>.GetIterator(enumerable);
        }
    }
}
