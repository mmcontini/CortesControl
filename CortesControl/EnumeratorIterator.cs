﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CortesControl
{
    /// <summary>
    /// Defines an implementation of <see cref="IIterator{T}"/> that
    /// works on any <see cref="IEnumerable{T}"/> instance by wrapping
    /// its enumerator.
    /// </summary>
    public class EnumeratorIterator<T> : IIterator<T>
    {
        private readonly IEnumerator<T> enumerator;
        private bool hasCurrent;

        public bool HasCurrent
        {
            get { return hasCurrent; }
        }

        public T Current
        {
            get { return enumerator.Current; }
        }

        public void MoveNext()
        {
            hasCurrent = enumerator.MoveNext();
        }

        private EnumeratorIterator(IEnumerator<T> enumerator)
        {
            this.enumerator = enumerator;
            this.hasCurrent = enumerator.MoveNext();
        }

        /// <summary>
        /// Gets an iterator for the specified enumerable.
        /// </summary>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static IIterator<T> GetIterator(IEnumerable<T> enumerable)
        {
            return new EnumeratorIterator<T>(enumerable.GetEnumerator());
        }

        public void Dispose()
        {
            enumerator.Dispose();
        }
    }
}
