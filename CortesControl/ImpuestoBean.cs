﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CortesControl
{
    /// <summary>
    /// Bean para impuestos.
    /// </summary>
    class ImpuestoBean
    {
        private string provincia;
        private string local;
        private Int32 importe;


        public ImpuestoBean(string provincia1, string local1, Int32 importe1) 
        {
            provincia = provincia1;
            local = local1;
            importe = importe1;
        }


        public string Provincia
        {
            get { return provincia; }
            set { provincia = value; }
        }

        public string Local
        {
            get { return local; }
            set { local = value; }
        }

        public Int32 Importe
        {
            get { return importe; }
            set { importe = value; }
        }

        public String getInfo()
        {
            return "provincia: " + provincia + " - local: " + local + " - importe: " + importe;
        } 

    }
}
