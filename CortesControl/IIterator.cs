﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CortesControl
{
    /// <summary>
    /// Defines methods for iteratating over a collection.
    /// </summary>
    /// <remarks>
    /// Sometimes it's advantageous to have a HasCurrent property. This 
    /// interface differs from <see cref="IEnumerator{T}>"/> in that
    /// <see cref="MoveNext"/> has no return value, and the value it would
    /// have returned is accessible from <see cref="HasCurrent"/>
    /// property.
    /// </remarks>
    public interface IIterator<out T> : IDisposable
    {
        bool HasCurrent { get; }
        T Current { get; }
        void MoveNext();
    }
}
