﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CortesControl
{
    class Program
    {

        /// <summary>
        /// Programa de ejecucion principal.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            List<ImpuestoBean> listaImpuestos = getListaImpuestos();

            string provAnt;
            string localAnt;
            int contLocal;
            int contImpuestos;
            int totalImpLocal = 0;
            int totImpProv;
            

            var iterator = listaImpuestos.GetIterator();
            while (iterator.HasCurrent)
            {
                Console.WriteLine("--------------------------------------------------");
                // Console.WriteLine(" +++ 1 +++ " + impuestoActual.getInfo() + " +++ 1 +++ ");
                // Se carga la variable que guardará la clave anterior
                provAnt = iterator.Current.Provincia; 
                
                // Se inicializa los contadores
                contLocal = 0;
                totImpProv = 0;

                // Primer corte de control
                while (iterator.HasCurrent && provAnt == iterator.Current.Provincia) 
                {
                    // Console.WriteLine(" +++ 2 +++ " + iterator.Current.getInfo() + " +++ 2 +++ ");
                    // Variable que guardará la clave anterior
                    localAnt = iterator.Current.Local; 

                    //inicializo contadores
                    totalImpLocal = 0;
                    contImpuestos = 0;

                    // Segundo corte de control
                    while (iterator.HasCurrent && provAnt == iterator.Current.Provincia && localAnt == iterator.Current.Local) 
                    {
                        // Console.WriteLine(" +++ 3 +++ " + iterator.Current.getInfo() + " +++ 3 +++ ");
                        totalImpLocal += iterator.Current.Importe;
                        contImpuestos++;
                        iterator.MoveNext();
                    }

                    //Cambió el Local
                    Console.WriteLine("Local " + localAnt + "- impuestos " + contImpuestos + "- total: " + totalImpLocal);

                    totImpProv += totalImpLocal;
                    contLocal++;
                }
                //Cambió la provincia
                Console.WriteLine("Prov - " + provAnt + "- cant locales " + contLocal + " total prov " + totImpProv);
            }
        }

        /// <summary>
        /// Se obtiene lista de impuestos precargada.
        /// </summary>
        /// <returns>List<ImpuestoBean></returns>
        static List<ImpuestoBean> getListaImpuestos()
        {
            return new List<ImpuestoBean>() { 
                new ImpuestoBean("Buenos Aires", "Local1", 10),
                new ImpuestoBean("Buenos Aires", "Local1", 20),
                new ImpuestoBean("Buenos Aires", "Local2", 30),
                new ImpuestoBean("Santa Fe", "Local1", 20),
                new ImpuestoBean("Santa Fe", "Local1", 30),
                new ImpuestoBean("Santa Fe", "Local2", 60),
                new ImpuestoBean("Santa Fe", "Local2", 50),
                new ImpuestoBean("Mendoza", "Local1", 20),
                new ImpuestoBean("Mendoza", "Local1", 30)
            };
        }
  
    }
}
